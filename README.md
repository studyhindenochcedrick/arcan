ÉVALUATION :

Créer un site web avec HTML, CSS et Bootstrap
GDWFSHTMLCSSBOOTEXAIII1A

Documents autorisés : Tous les documents sont autorisés.
Matériel autorisé : Tous les matériels sont autorisés.

Consignes à respecter pour votre examen
Cette évaluation du module Créer un site web avec HTML, CSS et Bootstrap vous évalue sous la forme
d'une épreuve écrite, portant sur des situations professionnelles concrètes abordant les compétences
suivantes du métier visé, selon le référentiel de certification :

• Réaliser une interface utilisateur web statique et adaptable
L’examen se déroule en 1 à 2 étapes de validation par le correcteur, avec le dépôt de vos livrables sur la plateforme, selon les étapes suivantes :

• 1ère étape : 1èrecorrection de votre examen par votre formateur
Si à l’issue de cette correction, vous n’avez pas obtenu une appréciation satisfaisante de votreformateur, vous pourrez bénéficier d’un dernier dépôt :

• 2ème correction éventuelle
Ce livrable est à remettre avec les dossiers correspondants à chaque partie de l’énoncé.
Notre équipe assurera le suivi pédagogique de votre dossier à travers l’interface exclusive
d'échange dans l’espace Evaluations.
Veuillez noter que les envois des différentes étapes qui seraient envoyés par un autre
moyen que cette interface ‘Evaluation’ de votre plateforme (courrier, email, fax) ne pourront être traités.
Seuls les documents transmis au format .doc, .docx ou au format .pdf ou .zip pourront être traités par notre serveur.

Livrable attendu pour l’examen de ce bloc
- Un site internet présente de nombreux intérêts pour une association, surtout en termes de
visibilité. Alors une association vous demande vos services dans la création d'un site vitrine.
- Un dépôt contenant le code source de votre rendu.
Contexte du Projet
Un site internet présente de nombreux intérêts pour une association, surtout en termes de visibilité.
Le site devra au minimum contenir 2 pages :
• Une page relative à l’association.
• Une page publicitaire des événements à l'échelle national (Vous choisissez le thème de l’événement, la date, les détails)
Bien entendu le site devra être responsive et à la demande du client l'interface devra être pensée mobile first !

Modalités Pédagogiques
Modalités / Restrictions :
1. Réalisation de l'interface en mobile first et utilisation des médias queries pour la rendre adaptable a tous
les formats d'écran
2. Un dépôt git avec des commit réguliers

Barème et critères d’évaluation
1. Interface utilisateur claire et bien pensée (5 points).
2. Structure sémantique html et utilisation des méta pour optimiser le référencement (5 points).
3. Intégration mobile first (5 points).
4. Utilisation de git / branch / commit réguliers avec des message clairs et concis (5 points).